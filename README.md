# palworld

Repository containing Kubernetes Mantifests for palworld.


## Notes:

### RCON

RCON is disabled by Palworld by default. Update `/game/Pal/Saved/Config/LinuxServer/PalWorldSettings.ini` to enable RCON and set an AdminPassword.